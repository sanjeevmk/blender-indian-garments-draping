import bpy,bmesh
import mathutils

class KeyFramesManager:	
	def __init(self,key):
		self.key = key
		self.key_index = 0

	def addKey():
		bops = BlenderOps()
		bops.addShapeKey()
		self.key_index+=1

	def setKeyValue(self,value,frame):
		bops = BlenderOps()
