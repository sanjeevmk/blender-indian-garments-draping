import bpy
import bmesh
import imp
import mathutils
import numpy as np

#_Garment = imp.load_source('Dhoti','/home/sanjeevmk/programs/indian_garments_addon/garment_generic.py')
#_Garment = imp.load_source('Dhoti','/home/sanjeevmk/programs/indian_garments_addon/garment_tuckback.py')
#_Garment = imp.load_source('Dhoti','/home/sanjeevmk/programs/indian_garments_addon/garment_kshatriya.py')
#_Garment = imp.load_source('Dhoti','/home/sanjeevmk/programs/indian_garments_addon/garment_wrap.py')
#_Garment = imp.load_source('Dhoti','/home/sanjeevmk/programs/indian_garments_addon/garment_overshoulder.py')
#_Garment = imp.load_source('Dhoti','/home/sanjeevmk/programs/indian_garments_addon/garment_simplepleat.py')
_Garment = imp.load_source('Dhoti','/home/sanjeevmk/programs/indian_garments_addon/garment_simple_fold.py')
_BlenderOps = imp.load_source('BlenderOps','/home/sanjeevmk/programs/indian_garments_addon/BlenderOps.py')

bl_info = {
	"name": "Drape Cloth",
	"author": "Sanjeev Mk",
	"version": (0, 1),
	"blender": (2, 73, 0),
	"location": "Edit panel of Tools tab, in Object mode, 3D View tools",
	"description": "Drape garments",
	"warning": "",
	"category": "Object"}

mapKeys = 0
num_free_ends = 0
class Pleats(bpy.types.Operator):
	bl_label = "Pleats"
	bl_idname = "mesh.pleats"

	def execute(self,context):
		gp = bpy.context.scene.grease_pencil
		num_pleats = len(gp.layers.active.active_frame.strokes)

		print("Pleats")
		_Garment.kp.numpleats = num_pleats
		print(_Garment.kp.numpleats)

		gp.layers.active.active_frame.clear()
		return {'FINISHED'}

class GetWidth(bpy.types.Operator):
	bl_label = "Waist Width"
	bl_idname = "mesh.cloth_width"

	def execute(self,context):
		gp = bpy.context.scene.grease_pencil
		latest_stroke = gp.layers.active.active_frame.strokes[-1]
		stroke_points = latest_stroke.points

		two_vertices = []
		two_vertices.append(stroke_points[0].co)
		two_vertices.append(stroke_points[-1].co)

		distance = (two_vertices[0]-two_vertices[1]).length
		_Garment.kp.waist_width = distance

		print("Width")
		print(_Garment.kp.waist_width)
	
		_Garment.kp.width_middle = ((two_vertices[0] + two_vertices[1]) / 2.0)

		if _Garment.kp.depth_middle != -1:
			_Garment.kp.origin =  mathutils.Vector([_Garment.kp.width_middle.x,_Garment.kp.depth_middle.y,_Garment.kp.width_middle.z])
			print(_Garment.kp.origin)

		gp.layers.active.active_frame.clear()
		return {'FINISHED'}

class GetDepth(bpy.types.Operator):
	bl_label = "Waist Depth"
	bl_idname = "mesh.cloth_depth"

	def execute(self,context):
		gp = bpy.context.scene.grease_pencil
		latest_stroke = gp.layers.active.active_frame.strokes[-1]
		stroke_points = latest_stroke.points
		
		two_vertices = []
		two_vertices.append(stroke_points[0].co)
		two_vertices.append(stroke_points[-1].co)

		distance = (two_vertices[0]-two_vertices[1]).length
		_Garment.kp.waist_depth = distance

		print("Depth")
		print(two_vertices[0],two_vertices[1],_Garment.kp.waist_depth)

		_Garment.kp.depth_middle = ((two_vertices[0] + two_vertices[1]) / 2.0)

		if _Garment.kp.width_middle != -1:
			_Garment.kp.origin =  mathutils.Vector([_Garment.kp.width_middle.x,_Garment.kp.depth_middle.y,_Garment.kp.width_middle.z])
			print(_Garment.kp.origin)

		gp.layers.active.active_frame.clear()
		return {'FINISHED'}

class GetHeight(bpy.types.Operator):
	bl_label = "Height"
	bl_idname = "mesh.cloth_height"

	def execute(self,context):
		gp = bpy.context.scene.grease_pencil
		latest_stroke = gp.layers.active.active_frame.strokes[-1]
		stroke_points = latest_stroke.points
		
		two_vertices = []
		two_vertices.append(stroke_points[0].co)
		two_vertices.append(stroke_points[-1].co)

		distance = (two_vertices[0]-two_vertices[1]).length
		_Garment.kp.height = distance

		print("height")
		print(two_vertices[0],two_vertices[1],_Garment.kp.height)

		gp.layers.active.active_frame.clear()
		return {'FINISHED'}

class GetBaseWidth(bpy.types.Operator):
	bl_label = "Bottom"
	bl_idname = "mesh.cloth_base_width"

	def execute(self,context):
		gp = bpy.context.scene.grease_pencil
		latest_stroke = gp.layers.active.active_frame.strokes[-1]
		stroke_points = latest_stroke.points
		
		two_vertices = []
		two_vertices.append(stroke_points[0].co)
		two_vertices.append(stroke_points[-1].co)

		distance = (two_vertices[0]-two_vertices[1]).length
		_Garment.kp.base_width = distance

		print("Base")
		print(two_vertices[0],two_vertices[1],_Garment.kp.base_width)

		gp.layers.active.active_frame.clear()
		return {'FINISHED'}

def smoothListGaussian(list,degree=5):  
	window=degree*2-1  
	weight=np.array([1.0]*window)  
	weightGauss=[]  
	for i in range(window):  
		i=i-degree+1  
		frac=i/float(window)  
		gauss=1/(np.exp((4*(frac))**2))  
		weightGauss.append(gauss)  
	weight=np.array(weightGauss)*weight  
	smoothed=[0.0]*(len(list)-window)  

	for i in range(len(smoothed)):  
		smoothed[i]=sum(np.array(list[i:i+window])*weight)/sum(weight)  

	return smoothed  

def movingaverage(interval, window_size):
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

class Capture(bpy.types.Operator):
	bl_label = "Capture"
	bl_idname = "mesh.capture"

	def execute(self,context):
		global num_free_ends
		gp = bpy.context.scene.grease_pencil
		top_stroke = gp.layers.active.active_frame.strokes[0]
		depth_stroke = gp.layers.active.active_frame.strokes[1]
		left_stroke = gp.layers.active.active_frame.strokes[2]
		bottom_stroke1 = gp.layers.active.active_frame.strokes[3]
		if(len(gp.layers.active.active_frame.strokes)==5):
			bottom_stroke2 = gp.layers.active.active_frame.strokes[4]

		width_points = top_stroke.points
		depth_points = depth_stroke.points
		height_points = left_stroke.points
		basewidth1_points = bottom_stroke1.points
		b1_x = []
		b1_z = []
		for p in basewidth1_points:
			b1_x.append(p.co.x)
			b1_z.append(p.co.z)
		#b1_x = smoothListGaussian(b1_x)	
		#b1_z = smoothListGaussian(b1_z)	
		b1_x = movingaverage(b1_x,len(basewidth1_points)/10)
		b1_z = movingaverage(b1_z,len(basewidth1_points)/10)
		_Garment.kp.origin = mathutils.Vector([(width_points[0].co.x+width_points[-1].co.x)/2.0,(depth_points[0].co.y+depth_points[-1].co.y)/2.0,(depth_points[0].co.z+depth_points[-1].co.z)/2.0])
		if(len(gp.layers.active.active_frame.strokes)==5):
			basewidth2_points = bottom_stroke2.points
			b2_x = []
			b2_z = []
			for p in basewidth2_points:
				b2_x.append(p.co.x)
				b2_z.append(p.co.z)
			#b2_x = smoothListGaussian(b2_x)	
			#b2_z = smoothListGaussian(b2_z)	
			b2_x = movingaverage(b2_x,len(basewidth2_points)/10)
			b2_z = movingaverage(b2_z,len(basewidth2_points)/10)

		_Garment.kp.waist_width = (width_points[0].co-width_points[-1].co).length
		width_vector = (width_points[-1].co - width_points[0].co)
		width_vector.normalize()
		_Garment.kp.waist_depth = (depth_points[0].co-depth_points[-1].co).length
		_Garment.kp.height = (height_points[0].co-height_points[-1].co).length
		numStrokes = len(gp.layers.active.active_frame.strokes)
		if numStrokes == 4:
			_Garment.kp.base_width = abs(basewidth1_points[0].co.x -basewidth1_points[-1].co.x)
		if numStrokes == 5:
			_Garment.kp.base_width = abs(basewidth1_points[0].co.x -basewidth2_points[-1].co.x)
			base2_vector = (basewidth2_points[-1].co - basewidth2_points[0].co)
			base2_vector.normalize()
			dot = width_vector.x*base2_vector.x + width_vector.y*base2_vector.y + width_vector.z*base2_vector.z
			print(dot)
		print("Base")
		print(width_points[0].co,width_points[-1].co,depth_points[0].co,depth_points[-1].co,_Garment.kp.origin)

		gp.layers.active.active_frame.clear()

		base1Length = -1
		base2Length = -1
		if numStrokes == 4 or numStrokes == 5:
			base1Length = 0
			for i in range(len(basewidth1_points)-1):
				base1Length += (basewidth1_points[i+1].co-basewidth1_points[i].co).length
		if numStrokes == 5:
			base2Length = 0
			for i in range(len(basewidth2_points)-1):
				base2Length += (basewidth2_points[i+1].co-basewidth2_points[i].co).length

		f1,b1_res,rank,sv,rcond = np.polyfit(np.array(b1_x),np.array(b1_z),1,full=True)
		if numStrokes == 5:
			f2,b2_res,rank,sv,rcond = np.polyfit(np.array(b2_x),np.array(b2_z),1,full=True)
		b1_res = b1_res[0]
		if numStrokes == 5:
			b2_res = b2_res[0]
		'''
		f,b1_res,rank,sv,rcond = np.polyfit(np.array(b1_x),np.array(b1_z),2,full=True)
		f,b2_res,rank,sv,rcond = np.polyfit(np.array(b2_x),np.array(b2_z),2,full=True)
		print(f,b1_res,b2_res)
		f,b1_res,rank,sv,rcond = np.polyfit(np.array(b1_x),np.array(b1_z),3,full=True)
		f,b2_res,rank,sv,rcond = np.polyfit(np.array(b2_x),np.array(b2_z),3,full=True)
		print(f,b1_res,b2_res)
		'''
		if numStrokes == 5 and base1Length > _Garment.kp.height:
			_Garment.style = "over shoulder"
			print(_Garment.style)
			_Garment.overShoulder = True	
			num_free_ends = 1
			return {'FINISHED'}
		if numStrokes == 5 and b1_res*10>1 and abs(f2[0])*10<1:
			_Garment.style = "tuckback"
			print(_Garment.style)
			_Garment.tuckBack = True
			num_free_ends = 2
			return {'FINISHED'}
		if numStrokes == 5 and b1_res*10>1 and b2_res*10>1:
			_Garment.style = "kshtriya"
			print(_Garment.style)
			_Garment.kshtriya = True
			num_free_ends = 2
			return {'FINISHED'}
		if numStrokes == 5 and abs(dot-0) < abs(dot-1):
			_Garment.style = "wrap"
			print(_Garment.style)
			_Garment.wrapDrape = True
			num_free_ends = 1
			return {'FINISHED'}
		if numStrokes == 5 and abs(f1[0])*10<1 and abs(f2[0])*10<1:
			_Garment.style = "simple pleat"
			print(_Garment.style)
			_Garment.simplePleat = True
			num_free_ends = 1
			return {'FINISHED'}
		_Garment.style = "simple fold"
		_Garment.simpleFold= True
		num_free_ends = 0
		return {'FINISHED'}

class Draper(bpy.types.Operator):
	bl_label = "Initial Position"
	bl_idname = "mesh.draper"

	def execute(self,context):
		global d
		global num_free_ends
		print(num_free_ends)
		num_free_ends = 0
		#d = _Garment.Dhoti(_Garment.kp.origin,_Garment.kp.waist_width,_Garment.kp.waist_depth,_Garment.kp.height,_Garment.kp.base_width,0,1,"Dhoti")
		if num_free_ends == 2:
			print("calling1")
			#d = _Garment.Dhoti(mathutils.Vector([0.0461, -0.0557, 3.4031]),0.9201306088043031,0.6042350213737295,3.3560525743750578,2.0880767914431684,0.05,3,"Dhoti")
			d = _Garment.Dhoti(_Garment.kp.origin,_Garment.kp.waist_width,_Garment.kp.waist_depth,_Garment.kp.height,_Garment.kp.base_width,0.1,context.scene.free_end_length,"Dhoti")
			#d = _Garment.Dhoti(mathutils.Vector([0.0461, -0.0557, 3.4031]),0.9201306088043031,0.6042350213737295,3.3560525743750578,2.0880767914431684,0.1,context.scene.free_end_length,"Dhoti")
		if num_free_ends == 1:
			print("calling2")
			#d = _Garment.Dhoti(_Garment.kp.origin,_Garment.kp.waist_width,_Garment.kp.waist_depth,_Garment.kp.height,_Garment.kp.base_width,0,context.scene.free_end_length,"Dhoti")
			d = _Garment.Dhoti(mathutils.Vector([0.0461, -0.0557, 3.4031]),0.9201306088043031,0.6042350213737295,3.3560525743750578,2.0880767914431684,0,context.scene.free_end_length,"Dhoti")
		if num_free_ends == 0:
			print("calling3")
			#d = _Garment.Dhoti(_Garment.kp.origin,_Garment.kp.waist_width,_Garment.kp.waist_depth,_Garment.kp.height,_Garment.kp.base_width,0.1,context.scene.free_end_length,"Dhoti")
			#d = _Garment.Dhoti(_Garment.kp.origin,_Garment.kp.waist_width,_Garment.kp.waist_depth,_Garment.kp.height,_Garment.kp.base_width,0.02,context.scene.free_end_length,"Dhoti")
			d = _Garment.Dhoti(mathutils.Vector([0.0461, -0.0557, 3.4031]),0.9201306088043031,0.6042350213737295,3.3560525743750578,2.0880767914431684,0,context.scene.free_end_length,"Dhoti")
			#d = _Garment.Dhoti(mathutils.Vector([0.0461, -0.0557, 3.4031]),0.9201306088043031,0.6042350213737295,3.3560525743750578,2.0880767914431684,0.05,context.scene.free_end_length,"Dhoti")
		d.num_free_ends = num_free_ends
		d.draw()

		return {'FINISHED'}

class GetPoints(bpy.types.Operator):
	bl_label = "Anchor Points"
	bl_idname = "mesh.points"
	pointsIndex = 0
	def execute(self,context):
		global d
		bops = _BlenderOps.BlenderOps()
		bops.addVertexGroup("Dhoti",d.vertex_group_index,"Anchor"+str(self.pointsIndex))
		d.vertex_group_index+=1
		self.pointsIndex+=1
		bops.deselectAll()
		return {'FINISHED'}

class GetMove(bpy.types.Operator):
	bl_label = "Capture Move"
	bl_idname = "mesh.move"

	def execute(self,context):
		global mapKeys
		bops = _BlenderOps.BlenderOps()

		gp = bpy.context.scene.grease_pencil

		xzstroke = gp.layers.active.active_frame.strokes[0]
		ystroke = gp.layers.active.active_frame.strokes[-1]
		xzstroke_points = xzstroke.points
		ystroke_points = ystroke.points
	
		xchange = xzstroke_points[-1].co.x - xzstroke_points[0].co.x
		zchange = xzstroke_points[-1].co.z - xzstroke_points[0].co.z
		ychange = ystroke_points[-1].co.y - ystroke_points[0].co.y

		_Garment.motionMap[mapKeys] = [[xchange,ychange,zchange],bops.getActiveGroup()]
		mapKeys+=1
		gp.layers.active.active_frame.clear()
		return {'FINISHED'}

class GetScale(bpy.types.Operator):
	bl_label = "Capture Scale"
	bl_idname = "mesh.scale"

	def execute(self,context):
		global mapKeys
		bops = _BlenderOps.BlenderOps()

		gp = bpy.context.scene.grease_pencil
		convergeStroke = gp.layers.active.active_frame.strokes[-1]

		stroke_points = convergeStroke.points

		scaleFactor = 1.0
		if abs(stroke_points[-1].co.x - stroke_points[0].co.x) >= abs(stroke_points[-1].co.y - stroke_points[0].co.y):
			if abs(stroke_points[-1].co.x - stroke_points[0].co.x) >= abs(stroke_points[-1].co.z - stroke_points[0].co.z):
				scaleFactor = stroke_points[-1].co.x/stroke_points[0].co.x
			else:
				scaleFactor = stroke_points[-1].co.z/stroke_points[0].co.z
		else:	
			if abs(stroke_points[-1].co.y - stroke_points[0].co.y) >= abs(stroke_points[-1].co.z - stroke_points[0].co.z):
				scaleFactor = stroke_points[-1].co.y/stroke_points[0].co.y
			else:
				scaleFactor = stroke_points[-1].co.z/stroke_points[0].co.z

		_Garment.motionMap[mapKeys] = [[scaleFactor],bops.getActiveGroup()]
		mapKeys+=1
		gp.layers.active.active_frame.clear()
		return {'FINISHED'}

class FreeEndPanel(bpy.types.Panel):
	"""A Custom Panel in the Viewport Toolbar"""
	bl_label = "Cloth free ends"
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'TOOLS'
	bl_category = 'Tools'

	def draw(self, context):
		layout = self.layout
		row = layout.row()
		row.label(text="Free end settings")	

		layout.prop(context.scene,"num_free_ends")
		layout.prop(context.scene,"free_end_length")

class Simulate(bpy.types.Operator):
	bl_label = "Drape"
	bl_idname = "mesh.drape"

	def execute(self,context):
		global d
		d.genericDrape()	
		return {'FINISHED'}

class ClothDimensions(bpy.types.Menu):
	bl_label = "Cloth Dimensions"
	bl_idname = "view3D.custom_menu"

	def draw(self, context):
		layout = self.layout
		'''
		layout.operator(GetWidth.bl_idname)
		layout.operator(GetDepth.bl_idname)
		layout.operator(GetHeight.bl_idname)
		layout.operator(GetBaseWidth.bl_idname)
		layout.operator(Draper.bl_idname)
		layout.operator(GetPoints.bl_idname)
		layout.operator(GetMove.bl_idname)
		layout.operator(GetScale.bl_idname)
		layout.operator(Simulate.bl_idname)
		'''
		layout.operator(Capture.bl_idname)
		layout.operator(Draper.bl_idname)

class DrapeStrokes(bpy.types.Menu):
	bl_label = "Drape Strokes"
	bl_idname = "view3D.custom_menu_1"

	def draw(self, context):
		layout = self.layout

		layout.operator(GetPoints.bl_idname)
		layout.operator(GetMove.bl_idname)
		layout.operator(GetScale.bl_idname)

def register():
	bpy.utils.register_class(GetWidth)
	bpy.utils.register_class(GetDepth)
	bpy.utils.register_class(GetHeight)
	bpy.utils.register_class(GetBaseWidth)
	bpy.utils.register_class(Draper)
	bpy.utils.register_class(Pleats)
	bpy.utils.register_class(GetPoints)
	bpy.utils.register_class(GetMove)
	bpy.utils.register_class(GetScale)
	bpy.utils.register_class(Simulate)
	bpy.utils.register_class(ClothDimensions)
	bpy.utils.register_class(Capture)

	bpy.utils.register_class(FreeEndPanel)
	bpy.types.Scene.num_free_ends = bpy.props.FloatProperty(name="Number of free ends",default=1)
	bpy.types.Scene.free_end_length = bpy.props.FloatProperty(name="Length of free ends",default=3)

	kc = bpy.context.window_manager.keyconfigs.user
	km = kc.keymaps['3D View']
	kmi = km.keymap_items.new('wm.call_menu','P','PRESS',shift=True)
	kmi.properties.name = ClothDimensions.bl_idname

	kmi.active = True

if __name__ == "__main__":
    register()
