import bpy,bmesh
import mathutils
from BlenderOps import BlenderOps

class facesManager:
	def __init__(self,faces):
		self.faces = faces
		self.indexed_faces = [[i,f] for i,f in zip(range(len(faces)),faces)]

	def sortByArea(self,reverse=False):
		self.indexed_faces.sort(key = lambda iff:iff[1].calc_area(),reverse=reverse)	

	def getSorted(self,start,end,step=1):
		return [iff[0] for iff in self.indexed_faces[start:end:step]]

	def selectSorted(self,start,end,step=1):
		bops = BlenderOps()
		bops.selectTheseFaces(self.faces,[iff[0] for iff in self.indexed_faces[start:end:step]])
