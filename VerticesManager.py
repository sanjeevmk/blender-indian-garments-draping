import bpy,bmesh
import mathutils
import imp
import mathutils
from BlenderOps import BlenderOps

def Stop():
	raise StopIteration

class verticesManager:
	def __init__(self,vertices):
		self.vertices = vertices
		self.indexed_vertices = [[i,v] for i,v in zip(range(len(vertices)),vertices)]		

	def sortOnX(self,reverse):
		self.indexed_vertices.sort(key = lambda iv : iv[1].co.x,reverse=reverse)

	def sortOnY(self,reverse):
		self.indexed_vertices.sort(key = lambda iv : iv[1].co.y,reverse=reverse)

	def sortOnZ(self,reverse):
		self.indexed_vertices.sort(key = lambda iv : iv[1].co.z,reverse=reverse)
	
	def sortOnXY(self,reverse,reverse2):
		if not reverse2:
			self.indexed_vertices.sort(key = lambda iv : (iv[1].co.x,iv[1].co.y),reverse=reverse)
		else:
			self.indexed_vertices.sort(key = lambda iv : (iv[1].co.x,-1*iv[1].co.y),reverse=reverse)

	def sortOnYX(self,reverse,reverse2):
		self.indexed_vertices.sort(key = lambda iv : (iv[1].co.y,iv[1].co.x),reverse=reverse)

	def sortOnYZ(self,reverse,reverse2):
		self.indexed_vertices.sort(key = lambda iv : (iv[1].co.y,iv[1].co.z),reverse=reverse)

	def sortOnZY(self,reverse,reverse2):
		if not reverse2:
			self.indexed_vertices.sort(key = lambda iv : (iv[1].co.z,iv[1].co.y),reverse=reverse)
		else:
			self.indexed_vertices.sort(key = lambda iv : (iv[1].co.z,-1*iv[1].co.y),reverse=reverse)

	def sortOnXZ(self,reverse,reverse2):
		self.indexed_vertices.sort(key = lambda iv : (iv[1].co.x,iv[1].co.z),reverse=reverse)
	
	def sortOnZX(self,reverse,reverse2):
		self.indexed_vertices.sort(key = lambda iv : (iv[1].co.z,iv[1].co.x),reverse=reverse)

	def gsortOnXY(self,reverse,reverse2):
		world = bpy.context.object.matrix_world
		if not reverse2:
			self.indexed_vertices.sort(key = lambda iv : ((world*iv[1].co).x,(world*iv[1].co).y),reverse=reverse)
		else:
			self.indexed_vertices.sort(key = lambda iv : ((world*iv[1].co).x,-1*(world*iv[1].co).y),reverse=reverse)

	def gsortOnYX(self,reverse,reverse2):
		world = bpy.context.object.matrix_world
		self.indexed_vertices.sort(key = lambda iv : ((world*iv[1].co).y,(world*iv[1].co).x),reverse=reverse)

	def gsortOnYZ(self,reverse,reverse2):
		world = bpy.context.object.matrix_world
		self.indexed_vertices.sort(key = lambda iv : ((world*iv[1].co).y,(world*iv[1].co).z),reverse=reverse)

	def gsortOnZY(self,world,reverse,reverse2):
		if not reverse2:
			self.indexed_vertices.sort(key = lambda iv : ((world*iv[1].co).z,(world*iv[1].co).y),reverse=reverse)
		else:
			self.indexed_vertices.sort(key = lambda iv : ((world*iv[1].co).z,-1*(world*iv[1].co).y),reverse=reverse)

	def gsortOnXZ(self,reverse,reverse2):
		world = bpy.context.object.matrix_world
		self.indexed_vertices.sort(key = lambda iv : ((world*iv[1].co).x,(world*iv[1].co).z),reverse=reverse)
	
	def gsortOnZX(self,reverse,reverse2):
		world = bpy.context.object.matrix_world
		self.indexed_vertices.sort(key = lambda iv : ((world*iv[1].co).z,(world*iv[1].co).x),reverse=reverse)

	def sortOnOneDimension(self,axis,reverse=False):
		if axis == (1,0,0):
			self.sortOnX(reverse)
	
		if axis == (0,1,0):
			self.sortOnY(reverse)
	
		if axis == (0,0,1):
			self.sortOnZ(reverse)

	def sortOnTwoDimensions(self,axis1,axis2,reverse=False,reverse2=False):
		if axis1 == (1,0,0) and axis2 == (0,1,0):
			self.sortOnXY(reverse,reverse2)
	
		if axis1 == (0,1,0) and axis2 == (1,0,0):
			self.sortOnYX(reverse,reverse2)
	
		if axis1 == (0,1,0) and axis2 == (0,0,1):
			self.sortOnYZ(reverse,reverse2)
	
		if axis1 == (0,0,1) and axis2 == (0,1,0):
			self.sortOnZY(reverse,reverse2)
	
		if axis1 == (1,0,0) and axis2 == (0,0,1):
			self.sortOnXZ(reverse,reverse2)
	
		if axis1 == (0,0,1) and axis2 == (1,0,0):
			self.sortOnZX(reverse,reverse2)

	def gsortOnTwoDimensions(self,world,axis1,axis2,reverse=False,reverse2=False):
		if axis1 == (1,0,0) and axis2 == (0,1,0):
			self.gsortOnXY(reverse,reverse2)
	
		if axis1 == (0,1,0) and axis2 == (1,0,0):
			self.gsortOnYX(reverse,reverse2)
	
		if axis1 == (0,1,0) and axis2 == (0,0,1):
			self.gsortOnYZ(reverse,reverse2)
	
		if axis1 == (0,0,1) and axis2 == (0,1,0):
			self.gsortOnZY(world,reverse,reverse2)
	
		if axis1 == (1,0,0) and axis2 == (0,0,1):
			self.gsortOnXZ(reverse,reverse2)
	
		if axis1 == (0,0,1) and axis2 == (1,0,0):
			self.gsortOnZX(reverse,reverse2)

	def selectVerticesByIndex(self,indices):
		self.vertices.ensure_lookup_table()
		bops = BlenderOps()
		bops.selectTheseVertices(self.vertices,indices)

	def getVerticesByIndex(self,indices):
		self.vertices.ensure_lookup_table()
		return [self.vertices[i] for i in indices]

	def getVerticesCoordinates(self,indices):
		self.vertices.ensure_lookup_table()
		return [bpy.context.object.matrix_world*self.vertices[i].co for i in indices]

	def selectSortedVerticesByIndex(self,indices):
		bops = BlenderOps()
		bops.selectTheseVertices([iv[1] for iv in self.indexed_vertices],indices)
	
	def getSortedVerticesByIndex(self,indices):
		bops = BlenderOps()
		return [self.indexed_vertices[index][1] for index in indices]

	def criteriaX(self,given_indices,limit,greater=True):
		selected = []
		for index in given_indices:
			if greater:
				if self.indexed_vertices[index][1].co.x > limit:
					selected.append(index)		
			if not greater:
				if self.indexed_vertices[index][1].co.x < limit:
					selected.append(index)		
		return selected
	
	def criteriaY(self,given_indices,limit,greater=True):
		selected = []
		for index in given_indices:
			if greater:
				if self.indexed_vertices[index][1].co.y > limit:
					selected.append(index)		
			if not greater:
				if self.indexed_vertices[index][1].co.y < limit:
					selected.append(index)		
		return selected
	
	def criteriaZ(self,given_indices,limit,greater=True):
		selected = []
		for index in given_indices:
			if greater:
				if self.indexed_vertices[index][1].co.z > limit:
					selected.append(index)		
			if not greater:
				if self.indexed_vertices[index][1].co.z < limit:
					selected.append(index)		
		return selected

	def selectSorted(self,start,end,step=1):
		self.vertices.ensure_lookup_table()
		bops = BlenderOps()
		bops.selectTheseVertices(self.vertices,[iv[0] for iv in self.indexed_vertices[start:end:step]])

	def getSorted(self,start,end):
		return [iv[1] for iv in self.indexed_vertices[start:end]]

	def selectTopInSorted(self,ordinate):
		bops = BlenderOps()
		if ordinate == (1,0,0):
			first = [self.indexed_vertices[0][0]]
			other_indices = list(iv_2[0] if iv_1[1].co.x == iv_2[1].co.x else Stop() for iv_1,iv_2 in zip(self.indexed_vertices,self.indexed_vertices[1:]))

		if ordinate == (0,1,0):
	
			other_indices = list(iv_2[0] if iv_1[1].co.y == iv_2[1].co.y else Stop() for iv_1,iv_2 in zip(self.indexed_vertices,self.indexed_vertices[1:]))

		if ordinate == (0,0,1):
			first = [self.indexed_vertices[0][0]]
			other_indices = list(iv_2[0] if iv_1[1].co.z == iv_2[1].co.z else Stop() for iv_1,iv_2 in zip(self.indexed_vertices,self.indexed_vertices[1:]))

		self.selectVerticesByIndex(first+other_indices)
	
	def getTopInSorted(self,ordinate):
		bops = BlenderOps()
		if ordinate == (1,0,0):
			first = [self.indexed_vertices[0][0]]
			other_indices = list(iv_2[0] if iv_1[1].co.x == iv_2[1].co.x else Stop() for iv_1,iv_2 in zip(self.indexed_vertices,self.indexed_vertices[1:]))

		if ordinate == (0,1,0):
			first = [self.indexed_vertices[0][0]]
			other_indices = list(iv_2[0] if iv_1[1].co.y == iv_2[1].co.y else Stop() for iv_1,iv_2 in zip(self.indexed_vertices,self.indexed_vertices[1:]))

		if ordinate == (0,0,1):
			first = [self.indexed_vertices[0][0]]
			other_indices = list(iv_2[0] if iv_1[1].co.z == iv_2[1].co.z else Stop() for iv_1,iv_2 in zip(self.indexed_vertices,self.indexed_vertices[1:]))

		return first+other_indices

	def howManyUptoX(self,limit,up=True):
		num = 0
		for v in self.indexed_vertices:
			if up and v[1].co.x <= limit:
				num+=1
			elif not up and v[1].co.x >= limit:
				num+=1
			else:
				break
		return num

	def howManyUptoY(self,limit,up=True):
		num = 0
		for v in self.indexed_vertices:
			if up and v[1].co.y <= limit:
				num+=1
			elif not up and v[1].co.y >= limit:
				num+=1
			else:
				break
		return num

	def howManyUptoZ(self,limit,up=True):
		num = 0
		for v in self.indexed_vertices:
			if up and v[1].co.z <= limit:
				num+=1
			elif not up and v[1].co.z >= limit:
				num+=1
			else:
				break
		return num

	def inBetweenX(self,mini,maxi):
		num = 0
		for v in self.vertices:
			if mini <= v.co.x and v.co.x <=maxi:
				num+=1
		return num
		
	def inBetweenY(self,mini,maxi):
		num = 0
		for v in self.vertices:
			if mini <= v.co.y and v.co.y <=maxi:
				num+=1
		return num
		
	def inBetweenZ(self,mini,maxi):
		num = 0
		for v in self.vertices:
			if mini <= v.co.z and v.co.z <=maxi:
				num+=1
		return num
	
	def totalArea(self,bm):
		area = 0
		for face in bm.faces:
			area += face.calc_area()

		return area

	def getGroupVerticesIndex(self,objname,groupname):
		index = 0
		indices = []
		ob = bpy.data.objects[objname]
		gi = ob.vertex_groups[groupname].index # get group index
		for v in ob.data.vertices:
			for g in v.groups:
				if g.group == gi: # compare with index in VertexGroupElement
					indices.append(index)
			index+=1

		return indices

	def selectChainFromVertex(self,direction,vertex):
		world = bpy.context.object.matrix_world

		bops = BlenderOps()
		current_vertex = vertex
		main_vertices = list(self.vertices)
		connected_v = current_vertex.link_edges
		bops.selectVertices([vertex])	
		for e in connected_v:
			other_v = e.other_vert(current_vertex)
			if direction == (0,0,1):
				if other_v.co.x == current_vertex.co.x and other_v.co.y == current_vertex.co.y:
					bops.selectVertices([other_v])
					break

			if direction == (0,1,0):
				if other_v.co.x == current_vertex.co.x and other_v.co.z == current_vertex.co.z:
					bops.selectVertices([other_v])
					break

			if direction == (0,0,1):
				if other_v.co.x == current_vertex.co.x and other_v.co.y == current_vertex.co.y:
					bops.selectVertices([other_v])
					break

		bops.selectEdgeLoop()

	def selectChainFromVertexByChange(self,direction,vertex):
		world = bpy.context.object.matrix_world

		bops = BlenderOps()
		current_vertex = vertex
		main_vertices = list(self.vertices)
		connected_v = current_vertex.link_edges
		bops.selectVertices([vertex])	
		for e in connected_v:
			other_v = e.other_vert(current_vertex)
			changey = (world*other_v.co).y-(world*current_vertex.co).y
			changex = (world*other_v.co).x-(world*current_vertex.co).x
			changez = (world*other_v.co).z-(world*current_vertex.co).z

			if direction == (0,1,0):
				if changey > 0 and abs(changey) > abs(changex) and abs(changey) > abs(changez):
					bops.selectVertices([other_v])
					break

			if direction == (0,0,-1):
				if changez < 0 and abs(changez) > abs(changex) and abs(changez) > abs(changey):
					bops.selectVertices([other_v])
					break

		bops.selectEdgeLoop()

	def walkChainFromVertexByChange(self,direction,vertex,n):
		world = bpy.context.object.matrix_world

		bops = BlenderOps()
		current_vertex = vertex
		keepWalking = True
		walkLength = 0
		while keepWalking:
			connected_v = current_vertex.link_edges
			found = False
			for e in connected_v:
				other_v = e.other_vert(current_vertex)
				changey = (world*other_v.co).y-(world*current_vertex.co).y
				changex = (world*other_v.co).x-(world*current_vertex.co).x
				changez = (world*other_v.co).z-(world*current_vertex.co).z

				if direction == (0,1,0):
					if changey > 0 and abs(changey) > abs(changex) and abs(changey) > abs(changez):
						walkLength += 1
						found = True
						current_vertex = other_v
						break

				if direction == (0,0,-1):
					if changez < 0 and abs(changez) > abs(changex) and abs(changez) > abs(changey):
						walkLength += 1
						found = True
						current_vertex = other_v
						break
	
			if not found or walkLength == n:
				keepWalking = False
		
		bops.selectVertices([other_v])	
		return walkLength

	def selectChainFromVertexEvery(self,direction,vertex,every,upto,exclude=False):
		world = bpy.context.object.matrix_world

		bops = BlenderOps()
		current_vertex = vertex

		chosenVertices = []
		if not exclude:
			bops.selectVertices([vertex])	
			visitedVertices = [current_vertex]
			chosenVertices.append(vertex)

		keepSelecting = True
		count = 0
		while keepSelecting:
			connected_v = current_vertex.link_edges
			found = False
			print("k")
			for e in connected_v:
				print("e")
				other_v = e.other_vert(current_vertex)
				changey = (world*other_v.co).y-(world*current_vertex.co).y
				changex = (world*other_v.co).x-(world*current_vertex.co).x
				changez = (world*other_v.co).z-(world*current_vertex.co).z
				if direction == -1:
					print("ch")
					if (world*other_v.co).y != (world*current_vertex.co).y or (world*other_v.co).z != (world*current_vertex.co).z or (world*other_v.co).x != (world*current_vertex.co).x and other_v not in visitedVertices:
						count+=1
						found = True
			
						if count%every == 0:
							bops.selectVertices([other_v])
							chosenVertices.append(other_v)
						current_vertex = other_v
						visitedVertices.append(other_v)
						break

				if direction == (0,1,1):
					if abs(changey)>0 and abs(changez)>0 and abs(changey)>abs(changex) and abs(changez)>abs(changex) and other_v not in visitedVertices:
						print(world*other_v.co)
						count+=1
						found = True
			
						if count%every == 0:
							bops.selectVertices([other_v])
							chosenVertices.append(other_v)
						current_vertex = other_v
						visitedVertices.append(other_v)
						break

				if direction == (0,1,0):
					if changey > 0 and abs(changey) > abs(changex) and abs(changey) > abs(changez) and other_v not in visitedVertices:
						count+=1
						found = True
			
						if count%every == 0:
							bops.selectVertices([other_v])
							chosenVertices.append(other_v)
						current_vertex = other_v
						visitedVertices.append(other_v)
						break

			if found == False or count == upto:
				keepSelecting = False

		return chosenVertices

	def reorderByConnected(self,direction,vert_list=None):
		bops = BlenderOps()
	def reorderByConnected(self,direction,vert_list=None):
		bops = BlenderOps()
		if not vert_list:
			vert_list = list(self.vertices)

		main_vertices = list(self.vertices)
		self.reordered = [main_vertices.index(vert_list[0])]
		current_vertex = vert_list[0]
	
		while len(self.reordered) < len(vert_list):
			connected_v = current_vertex.link_edges
			for e in connected_v:
				for v in e.verts:
					if direction == (1,0,0):
						if v.co.y == current_vertex.co.y and v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (0,1,0):
						if v.co.x == current_vertex.co.x and v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (0,0,1):
						if v.co.y == current_vertex.co.y and v.co.x == current_vertex.co.x:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (1,1,0):
						if v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

	def verticesDistance(self,v1,v2):
		world = bpy.context.object.matrix_world
		return (world*v2.co-world*v1.co).magnitude
	
	def lengthOfChain(self):
		length = 0
		for v1_i,v2_i in zip(self.reordered,self.reordered[1:]):
			length+=self.verticesDistance(self.vertices[v1_i],self.vertices[v2_i])

		return length
	
	def selectInChain(self):
		bops = BlenderOps()
		bops.selectTheseVertices(self.vertices,self.reordered)
	
	def getCenter(self):
		return int(len(self.reordered)/2) 

	def getLenOrdered(self):
		return len(self.reordered)

	def selectInOrdered(self,indices):
		for index in indices:
			self.selectVerticesByIndex([self.reordered[index]])

	def setCoordsInOrdered(self,index,co):
		self.vertices[self.reordered[index]].co = co

	def getInOrdered(self,index):
		return self.vertices[self.reordered[index]]

	def getReorderedIndices(self):
		return self.reordered

	def getUnitLength(self):
		length = self.verticesDistance(self.vertices[self.reordered[0]],self.vertices[self.reordered[1]])
		return length

	def reorderByConnected(self,direction,vert_list=None):
		bops = BlenderOps()
		if not vert_list:
			vert_list = list(self.vertices)

		main_vertices = list(self.vertices)
		self.reordered = [main_vertices.index(vert_list[0])]
		current_vertex = vert_list[0]
	
		while len(self.reordered) < len(vert_list):
			connected_v = current_vertex.link_edges
			for e in connected_v:
				for v in e.verts:
					if direction == (1,0,0):
						if v.co.y == current_vertex.co.y and v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (0,1,0):
						if v.co.x == current_vertex.co.x and v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (0,0,1):
						if v.co.y == current_vertex.co.y and v.co.x == current_vertex.co.x:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (1,1,0):
						if v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

	def verticesDistance(self,v1,v2):
		world = bpy.context.object.matrix_world
		return (world*v2.co-world*v1.co).magnitude
	
	def lengthOfChain(self):
		length = 0
		for v1_i,v2_i in zip(self.reordered,self.reordered[1:]):
			length+=self.verticesDistance(self.vertices[v1_i],self.vertices[v2_i])

		return length
	
	def selectInChain(self):
		bops = BlenderOps()
		bops.selectTheseVertices(self.vertices,self.reordered)
	
	def getCenter(self):
		return int(len(self.reordered)/2) 

	def getLenOrdered(self):
		return len(self.reordered)

	def selectInOrdered(self,indices):
		for index in indices:
			self.selectVerticesByIndex([self.reordered[index]])

	def getInOrdered(self,index):
		return self.vertices[self.reordered[index]]

	def getOrderedIndices(self):
		return [self.reordered[index]]

	def getReorderedIndices(self):
		return self.reordered

	def getUnitLength(self):
		length = self.verticesDistance(self.vertices[self.reordered[0]],self.vertices[self.reordered[1]])
		return length

	def reorderByConnected(self,direction,vert_list=None):
		bops = BlenderOps()
		if not vert_list:
			vert_list = list(self.vertices)

		main_vertices = list(self.vertices)
		self.reordered = [main_vertices.index(vert_list[0])]
		current_vertex = vert_list[0]
	
		while len(self.reordered) < len(vert_list):
			connected_v = current_vertex.link_edges
			for e in connected_v:
				for v in e.verts:
					if direction == (1,0,0):
						if v.co.y == current_vertex.co.y and v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (0,1,0):
						if v.co.x == current_vertex.co.x and v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (0,0,1):
						if v.co.y == current_vertex.co.y and v.co.x == current_vertex.co.x:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

					if direction == (1,1,0):
						if v.co.z == current_vertex.co.z:
							connected_index = main_vertices.index(v)
							if connected_index not in self.reordered:
								self.reordered.append(connected_index)
								current_vertex = v

	def verticesDistance(self,v1,v2):
		world = bpy.context.object.matrix_world
		return (world*v2.co-world*v1.co).magnitude
	
	def lengthOfChain(self):
		length = 0
		for v1_i,v2_i in zip(self.reordered,self.reordered[1:]):
			length+=self.verticesDistance(self.vertices[v1_i],self.vertices[v2_i])

		return length
	
	def selectInChain(self):
		bops = BlenderOps()
		bops.selectTheseVertices(self.vertices,self.reordered)
	
	def getCenter(self):
		return int(len(self.reordered)/2) 

	def getLenOrdered(self):
		return len(self.reordered)

	def selectInOrdered(self,indices):
		self.vertices.ensure_lookup_table()
		for index in indices:
			self.selectVerticesByIndex([self.reordered[index]])

	def getInOrdered(self,index):
		self.vertices.ensure_lookup_table()
		return self.vertices[self.reordered[index]]

	def getReorderedIndices(self):
		return self.reordered

	def getUnitLength(self):
		self.vertices.ensure_lookup_table()
		length = self.verticesDistance(self.vertices[self.reordered[0]],self.vertices[self.reordered[1]])
		return length

	def getSelectVertexCoord(self):
		for v in self.vertices:
			if v.select == True:
				return v.co

	def selectEvery(self,num):
		chosenVertices = []
		i = 0
		for v in self.vertices:
			if v.select == True:
				i+=1
				if i%num != 0:
					v.select = False
				else:
					chosenVertices.append(v)
		return chosenVertices

	def getEvery(self,num):
		chosenVertices = []
		i = 0
		for v in self.vertices:
			if v.select == True:
				i+=1
				if i%num != 0:
					v.select = False
				else:
					chosenVertices.append(v)
		return chosenVertices

	def getnthfrom(self,vertex,n,direction):
		world = bpy.context.object.matrix_world

		bops = BlenderOps()
		current_vertex = vertex
		connected_v = current_vertex.link_edges
			
		keepMoving = True
		count = 0
		while keepMoving:
			connected_v = current_vertex.link_edges
			found = False
			for e in connected_v:
				other_v = e.other_vert(current_vertex)
				if direction == (0,1,0):
					changey = (world*other_v.co).y-(world*current_vertex.co).y
					changex = (world*other_v.co).x-(world*current_vertex.co).x
					changez = (world*other_v.co).z-(world*current_vertex.co).z
					
					if changey > 0 and abs(changey) > abs(changex) and abs(changey) > abs(changez):
						count+=1
						found = True
						if count == n:
							return other_v
						else:
							current_vertex = other_v
							break

				if direction == (0,-1,0):
					changey = (world*other_v.co).y-(world*current_vertex.co).y
					changex = (world*other_v.co).x-(world*current_vertex.co).x
					changez = (world*other_v.co).z-(world*current_vertex.co).z
					
					if changey < 0 and abs(changey) > abs(changex) and abs(changey) > abs(changez):
						count+=1
						found = True
						if count == n:
							return other_v
						else:
							current_vertex = other_v
							break
			if not found:
				keepMoving = False
