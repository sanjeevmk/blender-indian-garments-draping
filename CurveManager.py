import bpy,bmesh
import mathutils

class CurveManager:
	def drawEllipse(self,location,width,depth,tiegap,name):
		bpy.ops.curve.primitive_nurbs_path_add(view_align=False,location = location)
		self.name = name
		obj = bpy.context.object

		bpy.context.active_object.name = name
	
		bpy.ops.object.mode_set(mode='EDIT')
		
		self.spline = obj.data.splines[0]
	
		# numbers below should be configurable based on size 
		self.spline.points[2].co.y += depth
		
		self.spline.points[1].co.x -= (width/2.0)
		self.spline.points[3].co.x += (width/2.0)
		
		self.spline.points[0].co.x = self.spline.points[2].co.x - (tiegap/2.0)
		self.spline.points[-1].co.x = self.spline.points[2].co.x + (tiegap/2.0)
		
		self.spline.points[0].co.y -= depth
		self.spline.points[-1].co.y -= depth

		for i in range(5):
			self.spline.points[i].co.y -= (depth/3.0)
