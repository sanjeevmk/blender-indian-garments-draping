import numpy as np

class vectorMath:
	def unit_vector(self,vector):
	    """ Returns the unit vector of the vector.  """
	    return vector / np.linalg.norm(vector)

	def angle(self,v1, v2):
		v1_u = self.unit_vector(v1)
		v2_u = self.unit_vector(v2)
		_angle = np.arccos(np.dot(v1_u, v2_u))
		if np.isnan(_angle):
			if (v1_u == v2_u).all():
			    return 0.0
			else:
			    return np.pi
		return _angle
