import bpy,bmesh
import mathutils
import imp
import sys
import pdb
#import BlenderOps
#import VerticesManager
#import VectorMath

bl_info = {"name": "Indian Garments Sim", "category": "Object"}
_BlenderOps = imp.load_source('BlenderOps','/home/sanjeevmk/programs/indian_garments_addon/BlenderOps.py')
_VerticesManager = imp.load_source('VerticesManager','/home/sanjeevmk/programs/indian_garments_addon/VerticesManager.py')
_FaceManager = imp.load_source('FaceManager','/home/sanjeevmk/programs/indian_garments_addon/FaceManager.py')
_CurveManager = imp.load_source('CurveManager','/home/sanjeevmk/programs/indian_garments_addon/CurveManager.py')
_KeyFramesManager = imp.load_source('KeyFramesManager','/home/sanjeevmk/programs/indian_garments_addon/KeyframesManager.py')
_VectorMath = imp.load_source('VectorMath','/home/sanjeevmk/programs/indian_garments_addon/VectorMath.py')
'''
_BlenderOps = imp.load_source('BlenderOps','/home/sanjeevmk/programs/indian_garments_addon/BlenderOps.py')
_VerticesManager = imp.load_source('VerticesManager','/home/sanjeevmk/programs/indian_garments_addon/VerticesManager.py')
_FaceManager = imp.load_source('FaceManager','/home/sanjeevmk/programs/indian_garments_addon/FaceManager.py')
_CurveManager = imp.load_source('CurveManager','/home/sanjeevmk/programs/indian_garments_addon/CurveManager.py')
_KeyFramesManager = imp.load_source('KeyFramesManager','/home/sanjeevmk/programs/indian_garments_addon/KeyframesManager.py')
_VectorMath = imp.load_source('VectorMath','/home/sanjeevmk/programs/indian_garments_addon/VectorMath.py')
'''
simpleDrape = False 
wrapDrape = False 
tuckBack = False 
simplePleat = True 
kshtriya = False
simpleFold = False 
overShoulder = False 
generic = False
style = None
class Simulate:
	def __init__(self):
		self.bops = _BlenderOps.BlenderOps()
		self.key = self.bops.createShapeKey()
		self.key_index = 0

	def translate(self,groupnames,value,constraint,init_time,end_time):
		self.bops.addShapeKey()	
		self.key_index+=1
		self.bops.setKeyValue(self.key,"Key " + str(self.key_index),1)
	
		self.bops.deselectAll()
		for groupname in groupnames:
			self.bops.selectGroupVertices(groupname)

		self.bops.translate(value = value,constraint=constraint)
	
		self.bops.setKeyValue(self.key,"Key " + str(self.key_index),0)
		self.bops.insertKeyframe(self.key,"Key " + str(self.key_index),init_time)

		self.bops.setKeyValue(self.key,"Key " + str(self.key_index),1)
		self.bops.insertKeyframe(self.key,"Key " + str(self.key_index),end_time)

	def scale(self,groupnames,value,constraint,init_time,end_time):
		self.bops.addShapeKey()	
		self.key_index+=1
		self.bops.setKeyValue(self.key,"Key " + str(self.key_index),1)
		
		self.bops.deselectAll()	
		for groupname in groupnames:
			self.bops.selectGroupVertices(groupname)

		self.bops.scale(value = value,constraint=constraint)
			
		self.bops.setKeyValue(self.key,"Key " + str(self.key_index),0)
		self.bops.insertKeyframe(self.key,"Key " + str(self.key_index),init_time)

		self.bops.setKeyValue(self.key,"Key " + str(self.key_index),1)
		self.bops.insertKeyframe(self.key,"Key " + str(self.key_index),end_time)

class CurveManager:
	def drawEllipse(self,location,width,depth,tiegap,name):
		bpy.ops.curve.primitive_nurbs_path_add(view_align=False,location = location)
		self.name = name
		obj = bpy.context.object

		bpy.context.active_object.name = name

		bpy.ops.object.mode_set(mode='EDIT')

		self.spline = obj.data.splines[0]

		# numbers below should be configurable based on size 
		self.spline.points[2].co.y += depth

		self.spline.points[1].co.x -= (width/3.0)
		self.spline.points[3].co.x += (width/3.0)

		self.spline.points[0].co.x = self.spline.points[2].co.x - (tiegap/2.0)
		self.spline.points[-1].co.x = self.spline.points[2].co.x + (tiegap/2.0)

		self.spline.points[0].co.y -= depth
		self.spline.points[-1].co.y -= depth

		for i in range(5):
			self.spline.points[i].co.y += depth/3.0
		self.spline.points[2].co.y += depth/3.0
			
class Dhoti:
	def __init__(self,location,waist_width,waist_depth,height,base_width,tiegap,pleatClothLength,name):
		self.waist_w= waist_width
		self.waist_d= waist_depth
		self.waist_h= height
		self.base_width = base_width

		self.location = location
		self.name = name
		self.tiegap = tiegap
		self.pleatClothLength = pleatClothLength
		self.vertex_group_index = 0
		self.num_free_ends = 0

	def genericDrape(self):
		bops = _BlenderOps.BlenderOps()
		sim = Simulate()

		bops.shadeSmooth()

		bops.deselectAll()
		bops.selectAllGroups()
		bops.addVertexGroup("Dhoti",self.vertex_group_index,"Allpin")
		self.vertex_group_index+=1

		bops.makeItCloth("Dhoti","Allpin")
		bops.deselectAll()

		frameNum = 0
		for k,v in motionMap.items():
			if len(v[0]) == 3:
				distance = mathutils.Vector(v[0]).length
				sim.translate([v[1]],tuple(v[0]),(False,False,False),frameNum,frameNum+(30*distance))
				frameNum += 30*distance
			if len(v[0]) == 1:
				sim.scale([v[1]],(v[0][0],v[0][0],v[0][0]),(False,False,False),frameNum,frameNum+(30*v[0][0]))
				frameNum += 30*v[0][0]

		bops.editModeShape(self.name)

	def wrapDrape(self):
		bops = _BlenderOps.BlenderOps()
		sim = Simulate()

		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
		plane_world = bops.getWorld(self.name)
		bops.deselectAll()
		bops.selectGroupVertices('WrapPin')

		start = plane_world*vm_plane.getSelectVertexCoord()
		sim.scale(['WaistPin'],(0.75,0.75,0.75),(True,True,False),0,50)
	
		sim.translate(['WrapPin'],(kp.waistLeftPoint +mathutils.Vector([self.pleatClothLength-(self.waist_w/2.0),0,0]))- start,(True,True,False),50,170)

		sim.translate(['WrapPin'],((kp.waistBackPoint+mathutils.Vector([0,self.pleatClothLength-self.waist_d,0]))-(kp.waistLeftPoint+mathutils.Vector([self.pleatClothLength-(self.waist_w/2.0),0,0]))),(False,False,False),170,290)

		sim.translate(['WrapPin'],(kp.waistRightPoint+mathutils.Vector([-self.pleatClothLength+(1.5*self.waist_w),0,0]))-(kp.waistBackPoint+mathutils.Vector([0,self.pleatClothLength-self.waist_d,0])),(False,False,False),290,410)

		sim.translate(['WrapPin'],((kp.waistFrontPoint+mathutils.Vector([0,-self.pleatClothLength+(1.5*self.waist_w),0]))-(kp.waistRightPoint+mathutils.Vector([-self.pleatClothLength+(1.5*self.waist_w),0,0]))),(False,False,False),410,530)

		sim.translate(['WrapPin'],(kp.waistLeftPoint+mathutils.Vector([0,-self.pleatClothLength+(2*self.waist_w),0]))-(kp.waistFrontPoint+mathutils.Vector([0,-self.pleatClothLength+(1.5*self.waist_w),0])),(False,False,False),530,650)

		sim.translate(['WrapPin'],(kp.waistLeftPoint)-(kp.waistLeftPoint+mathutils.Vector([0,-self.pleatClothLength+(2*self.waist_w),0])),(False,False,False),650,700)
		#bops.bake()

	def simplePleatDrape(self,vm):
		bops = _BlenderOps.BlenderOps()
		sim = Simulate()

		sim.translate(['creases'],(-self.waist_w/2.0,0,0),(True,False,False),0,50)

		bops.setPivotPoint('CURSOR',position=kp.waistFrontPoint)
		plane_world = bops.getWorld(self.name)
		sim.scale(['HorizontalScale','creases'],(0.0,self.pleatClothLength/200.0,0.1),(False,True,False),50,150)
		bops.setPivotPoint('ACTIVE_ELEMENT',kp.waistFrontPoint)

		sim.translate(['HorizontalScale','creases'],(self.waist_w/5.0,0,0),(True,False,False),150,240)

	def foldDrape(self):
		bops = _BlenderOps.BlenderOps()
		sim = Simulate()

		sim.translate(['FloorPin'],(0,0,0.8*self.waist_h),(False,False,True),0,100)

		bottom_scale_factor = float(self.base_width/self.waist_w)
		sim.scale(['FloorPin'],(1.0/bottom_scale_factor,1.0/bottom_scale_factor,1.0/bottom_scale_factor),(True,True,False),100,150)

	def overShoulderDrape(self):
		bops = _BlenderOps.BlenderOps()
		sim = Simulate()

		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
		plane_world = bops.getWorld(self.name)
		bops.deselectAll()
		bops.selectGroupVertices('WrapPin')

		start = plane_world*vm_plane.getSelectVertexCoord()
		sim.scale(['WaistPin'],(0.75,0.75,0.75),(True,True,False),0,50)
	
		sim.translate(['PinchScale'],(kp.waistLeftPoint +mathutils.Vector([self.pleatClothLength-(self.waist_w/2.0),0,0]))- start,(True,True,False),50,200)

		sim.translate(['PinchScale'],((kp.waistBackPoint+mathutils.Vector([0,self.pleatClothLength-self.waist_d,0]))-(kp.waistLeftPoint+mathutils.Vector([self.pleatClothLength-(self.waist_w/2.0),0,0]))),(False,False,False),200,350)

		sim.translate(['PinchScale'],(kp.waistRightPoint+mathutils.Vector([-self.pleatClothLength+(1.5*self.waist_w),0,0]))-(kp.waistBackPoint+mathutils.Vector([0,self.pleatClothLength-self.waist_d,0])),(False,False,False),350,500)

		sim.translate(['PinchScale'],((kp.waistFrontPoint+mathutils.Vector([0,-self.pleatClothLength/2.0,0]))-(kp.waistRightPoint+mathutils.Vector([-self.pleatClothLength+(1.5*self.waist_w),0,0]))),(False,False,False),500,650)

		#sim.translate(['PinchScale'],(kp.waistLeftPoint+mathutils.Vector([0,-self.pleatClothLength+(2*self.waist_w),0]))-(kp.waistFrontPoint+mathutils.Vector([0,-self.pleatClothLength/2.0,0])),(False,False,False),530,650)

		#sim.translate(['PinchScale'],(kp.waistLeftPoint)-(kp.waistLeftPoint+mathutils.Vector([0,-self.pleatClothLength+(2*self.waist_w),0])),(False,False,False),650,700)
	
		sim.scale(['PinchScale'],(0.05,0.05,0.05),(False,False,False),600,630)

		#sim.translate(['PinchScale'],(-0.15*(self.waist_w/2.0),0, kp.shoulderPoint.z-(kp.waistRightPoint.z-(self.waist_h/2.0))),(True,False,True),730,800)
		sim.translate(['PinchScale'],kp.shoulderPoint-(kp.waistFrontPoint+mathutils.Vector([0,-self.pleatClothLength/2.0,-self.waist_h/2.0])),(False,False,False),630,750)
		sim.translate(['PinchScale'],(0,0.5*(kp.shoulderPoint - kp.waistRightPoint).magnitude,0),(False,True,False),750,820)
		sim.translate(['PinchScale'],(0,kp.waistRightPoint.y-(kp.shoulderPoint.y+0.5*(kp.shoulderPoint - kp.waistRightPoint).magnitude),(kp.waistRightPoint.z-0.1*self.waist_h)-kp.shoulderPoint.z),(False,False,True),820,970)

	def kdrape(self,vm):
		bops = _BlenderOps.BlenderOps()
		sim = Simulate()

		sim.scale(['RPinchScale'],(0.05,0.05,0.05),(False,False,False),0,30)

		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm = _VerticesManager.verticesManager(self.bm_p.verts)
		indices = vm.getGroupVerticesIndex('Dhoti','RPinchScale')
		vm.selectVerticesByIndex(indices)
		pleat_positions = vm.getVerticesCoordinates(indices)

		old_y = pleat_positions[0].y + (kp.tuckbackbottom.y - pleat_positions[0].y)/2.0 	
		nextjump_y = (kp.tuckbackbottom.y - pleat_positions[0].y)/2.0 
		nextjump_z = ((kp.tuckbackbottom.z-1.5) - kp.midHeightApprox)

		sim.translate(['RPinchScale'],(0,nextjump_y,nextjump_z),(False,True,True),30,70)
		sim.scale(['LPinchScale'],(0.05,0.05,0.05),(False,False,False),30,60)

		nextjump_y = (kp.tuckbackbottom.y+1.0) - old_y
		sim.translate(['RPinchScale'],(0,nextjump_y,0),(False,True,False),70,100)

		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm = _VerticesManager.verticesManager(self.bm_p.verts)
		indices = vm.getGroupVerticesIndex('Dhoti','LPinchScale')
		vm.selectVerticesByIndex(indices)
		pleat_positions = vm.getVerticesCoordinates(indices)

		old_y = pleat_positions[0].y + (kp.tuckbackbottom.y - pleat_positions[0].y)/2.0 	
		nextjump_y = (kp.tuckbackbottom.y - pleat_positions[0].y)/2.0 
		nextjump_z = ((kp.tuckbackbottom.z-1.5) - kp.midHeightApprox)
		sim.translate(['LPinchScale'],(0,nextjump_y,nextjump_z),(False,True,True),60,100)

		nextjump_z = ((kp.waistBackPoint.z)-(kp.tuckbackbottom.z-1.5))
		sim.translate(['RPinchScale'],(0,0,nextjump_z),(False,False,True),100,390)

		nextjump_y = (kp.tuckbackbottom.y+1.0) - old_y
		sim.translate(['LPinchScale'],(0,nextjump_y,0),(False,True,False),100,130)
		
		nextjump_y = ((kp.waistBackPoint.y)-(kp.waistBackPoint.y+1.0))
		sim.translate(['RPinchScale'],(0,nextjump_y,0),(False,True,False),390,420)	

		nextjump_z = ((kp.waistBackPoint.z)-(kp.tuckbackbottom.z-1.5))
		sim.translate(['LPinchScale'],(0,0,nextjump_z),(False,False,True),130,420)

		nextjump_y = ((kp.waistBackPoint.y-0.25)-(kp.waistBackPoint.y+1.0))
		sim.translate(['LPinchScale'],(0,nextjump_y,0),(False,True,False),420,450)	

	def drape(self,vm):
		bops = _BlenderOps.BlenderOps()
		sim = Simulate()

		sim.scale(['PinchScale'],(0.05,0.05,0.05),(False,False,False),0,30)

		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm = _VerticesManager.verticesManager(self.bm_p.verts)
		indices = vm.getGroupVerticesIndex('Dhoti','PinchScale')
		vm.selectVerticesByIndex(indices)
		pleat_positions = vm.getVerticesCoordinates(indices)

		old_y = pleat_positions[0].y + (kp.tuckbackbottom.y - pleat_positions[0].y)/2.0 	
		nextjump_y = (kp.tuckbackbottom.y - pleat_positions[0].y)/2.0 
		nextjump_z = ((kp.tuckbackbottom.z-1.5) - kp.midHeightApprox)

		sim.translate(['PinchScale'],(0,nextjump_y,nextjump_z),(False,True,True),30,70)

		nextjump_y = (kp.tuckbackbottom.y+2.0) - old_y
		sim.translate(['PinchScale'],(0,nextjump_y,0),(False,True,False),70,100)

		nextjump_z = ((kp.waistBackPoint.z)-(kp.tuckbackbottom.z-1.5))
		sim.translate(['PinchScale'],(0,0,nextjump_z),(False,False,True),100,450)
		
		nextjump_y = ((kp.waistBackPoint.y)-(kp.waistBackPoint.y+2.0))
		sim.translate(['PinchScale'],(0,nextjump_y,0),(False,True,False),450,480)	

		sim.translate(['CreaseScale'],(self.waist_w/2.0,0,0),(True,False,False),480,510)

		bops.setPivotPoint('CURSOR',position=kp.waistFrontPoint)
		sim.scale(['NonCreaseScale','CreaseScale'],(0.0,self.pleatClothLength/100.0,0.1),(False,True,False),510,550)
		bops.setPivotPoint('ACTIVE_ELEMENT',kp.waistFrontPoint)

	def wrapPlaneOverEllipse(self,vm_plane,vm):
		bops = _BlenderOps.BlenderOps()
		bops.deselectAll()
		center_index_plane = vm_plane.getCenter()
		center_index_ellipse = vm.getCenter()

		plane_unit = vm_plane.getUnitLength()
		length_along_plane = plane_unit

		plane_left = list(range(center_index_plane-1,-1,-1))
		plane_right = list(range(center_index_plane+1,vm.getLenOrdered()))
		
		bops.select("Frame")
		ellipse_right = range(center_index_ellipse+1,vm.getLenOrdered())
		ellipse_left = range(center_index_ellipse-1,-1,-1)
		
		length_along_ell_left = vm.verticesDistance(vm.getInOrdered(ellipse_left[0]),vm.getInOrdered(center_index_ellipse))
		length_along_ell_right = vm.verticesDistance(vm.getInOrdered(ellipse_right[0]),vm.getInOrdered(center_index_ellipse))

		diff_left = length_along_plane - length_along_ell_left
		diff_right = length_along_plane - length_along_ell_right

		index_left_ell = 1
		index_right_ell = 1

		left_ellipse_indices = []
		right_ellipse_indices = []

		length_along_plane = 0
		for left_p,right_p in zip(plane_left,plane_right):
			if index_left_ell == len(ellipse_left) or index_right_ell == len(ellipse_right):
				break

			length_along_plane += plane_unit	
		
			length_along_ell_left += vm.verticesDistance(vm.getInOrdered(ellipse_left[index_left_ell]),vm.getInOrdered(ellipse_left[index_left_ell-1]))
			length_along_ell_right += vm.verticesDistance(vm.getInOrdered(ellipse_right[index_right_ell]),vm.getInOrdered(ellipse_right[index_right_ell-1]))

			index_left_ell+=1
			index_right_ell+=1

			diff_left_new = length_along_plane - length_along_ell_left
			diff_right_new = length_along_plane - length_along_ell_right
			
			while diff_left_new <= diff_left and diff_left_new >= 0 and index_left_ell < len(ellipse_left):
				v_ell_left = vm.getInOrdered(ellipse_left[index_left_ell])		
				v_ell_left_prev = vm.getInOrdered(ellipse_left[index_left_ell-1])		

				length_along_ell_left += vm.verticesDistance(v_ell_left,v_ell_left_prev)
			
				diff_left = diff_left_new
				diff_left_new = length_along_plane-length_along_ell_left 

				index_left_ell+=1

			left_ellipse_indices.append(ellipse_left[index_left_ell-1])			
			diff_left = diff_left_new + plane_unit

			while diff_right_new <= diff_right and diff_right_new >= 0 and index_right_ell < len(ellipse_right):
				v_ell_right = vm.getInOrdered(ellipse_right[index_right_ell])		
				v_ell_right_prev = vm.getInOrdered(ellipse_right[index_right_ell-1])		

				length_along_ell_right += vm.verticesDistance(v_ell_right,v_ell_right_prev)

				diff_right = diff_right_new
				diff_right_new = length_along_plane-length_along_ell_right 
				
				index_right_ell+=1

			right_ellipse_indices.append(ellipse_right[index_right_ell-1])
			diff_right = diff_right_new + plane_unit

		bops.update()
		ell_world = bops.getWorld("Frame")
		plane_world = bops.getWorld(self.name)

		left_indices =	list(range(center_index_plane-1,0,-1)) + [0]
		right_indices =	list(range(center_index_plane+1,vm.getLenOrdered()))

		bops.select(self.name)

		for p_left_i,e_left_i in zip(left_indices,left_ellipse_indices):
			left_p_vert = vm_plane.getInOrdered(p_left_i)
			left_e_vert = vm.getInOrdered(e_left_i)	

			p_vert_co = plane_world*left_p_vert.co

			bops.deselectAll()	
			vm_plane.selectChainFromVertex((0,1,0),left_p_vert)

			bops.select("Frame")
			vm.selectInOrdered([e_left_i])
			e_vert_co = ell_world*vm.getSelectVertexCoord()
			bops.deselectAll()

			trans = e_vert_co - p_vert_co

			bops.select(self.name)
		
			bops.translate(value=trans,constraint=(False,False,False))

		bops.select(self.name)

		for p_right_i,e_right_i in zip(right_indices,right_ellipse_indices):
			right_p_vert = vm_plane.getInOrdered(p_right_i)
			right_e_vert = vm.getInOrdered(e_right_i)	

			p_vert_co = plane_world*right_p_vert.co

			bops.deselectAll()	
			vm_plane.selectChainFromVertex((0,1,0),right_p_vert)

			bops.select("Frame")
			vm.selectInOrdered([e_right_i])
			e_vert_co = ell_world*vm.getSelectVertexCoord()
			bops.deselectAll()

			trans = e_vert_co - p_vert_co

			bops.select(self.name)
		
			bops.translate(value=trans,constraint=(False,False,False))

		bops.deselectAll()	
		bops.deselect(self.name)
		bops.delete("Frame")

	def total_length(self,bm):
		length=0
		for e in bm.edges:
			length += e.calc_length()

		return length

	def draw(self):
		print(style)
		cm = CurveManager()
		bops = _BlenderOps.BlenderOps()
		vmath = _VectorMath.vectorMath()

		cm.drawEllipse(self.location,self.waist_w,self.waist_d,self.tiegap,"Frame")
		#bops.translate(value=(0,(self.waist_d/2.0),0),constraint=(False,True,False))
		bops.convertCurveToMesh()
	
		bops.faceSubdivide(7)
	
		self.bm = bmesh.from_edit_mesh(bpy.context.object.data)
		vm = _VerticesManager.verticesManager(self.bm.verts)
		vm.reorderByConnected((1,1,0))
		self.frame_length = self.total_length(self.bm)
				
		vm.sortOnOneDimension((0,1,0),reverse=True)
		kp.tuckbackpoint = bpy.context.object.matrix_world*vm.getSorted(0,1)[0].co

		_half_pi = 22.0/14.0
	
		self.plane_length = self.frame_length

		bops.createPlane(self.location-mathutils.Vector([0,self.location.y-kp.tuckbackpoint.y,self.waist_h/2.0]),(_half_pi,0,0),self.plane_length,self.waist_h,self.name)
		bops.faceSubdivide(7)

		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
		vm_plane.sortOnOneDimension((0,1,0),reverse=True)
		top_indices = vm_plane.getTopInSorted((0,1,0))
		top_vertices = vm_plane.getVerticesByIndex(top_indices)
		vm_plane.reorderByConnected((1,0,0),vert_list = top_vertices)

		self.wrapPlaneOverEllipse(vm_plane,vm)
		bops.selectObj(self.name)
		bops.setMode('EDIT')

		plane_world = bops.getWorld(self.name)

		bops.deselectAll()
		vm_plane.selectVerticesByIndex(top_indices)
		bops.addVertexGroup("Dhoti",self.vertex_group_index,"WaistPin")
		self.vertex_group_index+=1
		bops.deselectAll()

		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
		vm_plane.sortOnOneDimension((1,0,0))
		top_right_waist = vm_plane.getSorted(0,1)
		kp.waistRightPoint = plane_world*top_right_waist[0].co
	
		bops.deselectAll()
		vm_plane.sortOnTwoDimensions((1,0,0),(0,1,0),reverse=True)
		top_left_waist = vm_plane.selectSorted(0,1)
		kp.waistLeftPoint = plane_world*vm_plane.getSelectVertexCoord()

		bops.deselectAll()
		vm_plane.sortOnTwoDimensions((0,0,1),(0,1,0),reverse2=True)
		top_back_waist = vm_plane.getSorted(0,1)
		kp.waistBackPoint = plane_world*top_back_waist[0].co
		kp.midHeightApprox = kp.waistBackPoint.z - self.waist_h/2.0

		bops.deselectAll()
		vm_plane.sortOnTwoDimensions((0,0,1),(0,1,0),reverse=True)
		top_front_waist = vm_plane.getSorted(0,1)
		kp.waistFrontPoint = plane_world*top_front_waist[0].co

		bops.deselectAll()
		vm_plane.sortOnOneDimension((0,1,0))
		vm_plane.selectSorted(0,1)
		kp.tuckbackbottom = plane_world*vm_plane.getSelectVertexCoord()

		bops.deselectAll()
		
		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

		vm_plane.sortOnOneDimension((0,1,0))
		vm_plane.selectSorted(0,len(top_vertices))

		if simpleFold:
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"FloorPin")
			self.vertex_group_index+=1

		bottom_scale_factor = float(self.base_width/self.waist_w)
		bops.scale((bottom_scale_factor,bottom_scale_factor,bottom_scale_factor),(True,True,False),proportional='ENABLED',proportional_size=self.waist_h)
	
		if generic:
			bops.deselectAll()
			if self.num_free_ends == 1:	
				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				vm_plane.sortOnOneDimension((0,0,1),reverse=True)
				vm_plane.selectSorted(0,1)
				vm_plane.sortOnOneDimension((0,1,0),reverse=True)
				vm_plane.selectSorted(0,1)
				bops.shortestPath()

				bops.addVertexGroup("Dhoti",self.vertex_group_index,"RightPleatUnextended")
				self.vertex_group_index+=1

				bops.selectGroupVertices("RightPleatUnextended")

				for i in range(80):
					bops.extrudeOneAxis((0,1,0),-1*(self.pleatClothLength/80.0))

				bops.deselectAll()
				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
				plane_world = bops.getWorld(self.name)
				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
				bops.deselectAll()
				vm_plane.sortOnOneDimension((0,1,0),reverse=True)

				vm_plane.selectSorted(0,80)
				bops.removeFromVertexGroup("WaistPin")

			if self.num_free_ends == 2:	
				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				vm_plane.sortOnOneDimension((0,0,1),reverse=True)
				vm_plane.selectSorted(0,1)
				vm_plane.sortOnOneDimension((0,1,0),reverse=True)
				vm_plane.selectSorted(0,1)
				bops.shortestPath()

				bops.addVertexGroup("Dhoti",self.vertex_group_index,"RightPleatUnextended")
				self.vertex_group_index+=1
				bops.deselectAll()

				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
				bops.deselectAll()
				vm_plane.sortOnOneDimension((0,1,0),reverse=True)
				vm_plane.selectSorted(1,2)
				vm_plane.sortOnOneDimension((0,0,1),reverse=True)
				vm_plane.selectSorted(1,2)

				bops.shortestPath()
				bops.addVertexGroup("Dhoti",self.vertex_group_index,"LeftPleatUnextended")
				self.vertex_group_index+=1

				bops.deselectAll()
				bops.selectGroupVertices("RightPleatUnextended")
				bops.selectGroupVertices("LeftPleatUnextended")

				for i in range(80):
					bops.extrudeOneAxis((0,1,0),-1*(self.pleatClothLength/80.0))
				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
				plane_world = bops.getWorld(self.name)
				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				bops.deselectAll()
				vm_plane.sortOnOneDimension((0,1,0),reverse=True)
				vm_plane.selectSorted(0,160)
				bops.removeFromVertexGroup("WaistPin")
				bops.deselectAll()

			return 0

		if simpleDrape:
			bops.shadeSmooth()
			bops.makeItCloth("Dhoti","WaistPin")
			bops.deselectAll()
			return 0
		
		if simpleFold:
			bops.selectGroupVertices("WaistPin")
			bops.selectGroupVertices("FloorPin")

			bops.addVertexGroup("Dhoti",self.vertex_group_index,"AllPin")
			self.vertex_group_index+=1

			bops.shadeSmooth()
			bops.makeItCloth("Dhoti","AllPin")

			self.foldDrape()
			bops.editModeShape(self.name)
			bops.deselectAll()
			return 0

		bops.deselectAll()

		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

		vm_plane.sortOnOneDimension((0,0,1),reverse=True)
		vm_plane.selectSorted(0,1)
		vm_plane.sortOnOneDimension((0,1,0),reverse=True)
		vm_plane.selectSorted(0,1)
		bops.shortestPath()

		bops.addVertexGroup("Dhoti",self.vertex_group_index,"RightPleatUnextended")
		self.vertex_group_index+=1

		if tuckBack or kshtriya:
			self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
			vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
			bops.deselectAll()
			vm_plane.sortOnOneDimension((0,1,0),reverse=True)
			vm_plane.selectSorted(1,2)
			vm_plane.sortOnOneDimension((0,0,1),reverse=True)
			vm_plane.selectSorted(1,2)

			bops.shortestPath()
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"LeftPleatUnextended")
			self.vertex_group_index+=1
		
		if wrapDrape:
			self.pleatClothLength = self.frame_length*1.2
		if overShoulder:
			self.pleatClothLength = self.frame_length*1.2 + 2*(kp.shoulderPoint - kp.waistRightPoint).magnitude
		elif tuckBack:
			self.pleatClothLength = self.waist_h/2.0

		bops.selectGroupVertices("RightPleatUnextended")
		if tuckBack or kshtriya:
			bops.selectGroupVertices("LeftPleatUnextended")

		if not overShoulder:
			for i in range(80):
				bops.extrudeOneAxis((0,1,0),-1*(self.pleatClothLength/80.0))
		else:
			for i in range(240):
				bops.extrudeOneAxis((0,1,0),-1*(self.pleatClothLength/240.0))
		
		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)
		plane_world = bops.getWorld(self.name)
		self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
		vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

		bops.deselectAll()
		vm_plane.sortOnOneDimension((0,1,0),reverse=True)
		if simplePleat or wrapDrape or generic:
			vm_plane.selectSorted(0,80)
		if tuckBack or kshtriya:
			vm_plane.selectSorted(0,160)
		if overShoulder:
			vm_plane.selectSorted(0,240)

		bops.removeFromVertexGroup("WaistPin")

		if wrapDrape:
			bops.deselectAll()
			vm_plane.sortOnOneDimension((0,1,0),reverse=True)
			vm_plane.selectSorted(0,1)
			vm_plane.sortOnOneDimension((0,0,1),reverse=True)
			vm_plane.selectSorted(0,1)
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"WrapPin")
			self.vertex_group_index+=1
			'''
			bops.deselectAll()
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"BottomWrapPin")
			self.vertex_group_index+=1
			'''
			bops.deselectAll()
			bops.selectGroupVertices("WaistPin")
			bops.selectGroupVertices("WrapPin")
			#bops.selectGroupVertices("BottomWrapPin")

			bops.addVertexGroup("Dhoti",self.vertex_group_index,"DhotiPin")
			self.vertex_group_index+=1

			bops.shadeSmooth()
		
			bops.makeItCloth("Dhoti","DhotiPin")
			self.wrapDrape()
			bops.editModeShape(self.name)
			return 0

		if overShoulder:
			self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
			vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

			vm_plane.sortOnOneDimension((0,0,1),reverse=True)
			fromHere = vm_plane.getSorted(0,1)
			chosenVertices = vm_plane.selectChainFromVertexEvery(-1,fromHere[0],16,128)

			bops.deselectAll()
			vm_plane.sortOnOneDimension((0,1,0),reverse=True)
			vm_plane.selectSorted(0,1)
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"WrapPin")
			self.vertex_group_index+=1

			bops.deselectAll()
			bops.selectVertices(chosenVertices)	
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"PinchScale")
			self.vertex_group_index+=1
			bops.deselectAll()
		
			bops.deselectAll()
			bops.selectGroupVertices("WaistPin")
			bops.selectGroupVertices("PinchScale")

			bops.addVertexGroup("Dhoti",self.vertex_group_index,"DhotiPin")
			self.vertex_group_index+=1

			bops.shadeSmooth()
		
			bops.makeItCloth("Dhoti","DhotiPin")
			self.overShoulderDrape()
			bops.editModeShape(self.name)
			return 0

		if tuckBack:
			chosenVertices = vm_plane.selectEvery(160/10)
			numvPleatVertices = len(chosenVertices)
			bops.deselectAll()

			for i,j in zip(range(numvPleatVertices-1,-1,-2),range(numvPleatVertices-2,-1,-2)):
				vm_plane.selectChainFromVertexByChange((0,0,-1),chosenVertices[i])

				bops.addVertexGroup("Dhoti",self.vertex_group_index,"NonCreaseScale"+str(i))
				self.vertex_group_index+=1
				bops.deselectAll()

				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				vm_plane.sortOnOneDimension((0,1,0),reverse=True)
				vm_plane.selectSorted(0,160)
				chosenVertices = vm_plane.selectEvery(int(160/10))
				bops.deselectAll()

				vm_plane.selectChainFromVertexByChange((0,0,-1),chosenVertices[j])

				bops.addVertexGroup("Dhoti",self.vertex_group_index,"CreaseScale"+str(i))
				self.vertex_group_index+=1
				bops.deselectAll()

				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				vm_plane.sortOnOneDimension((0,1,0),reverse=True)
				vm_plane.selectSorted(0,160)
				chosenVertices = vm_plane.selectEvery(int(160/10))
				bops.deselectAll()

			for i in range(numvPleatVertices-1,-1,-2):
				bops.selectGroupVertices("NonCreaseScale"+str(i))
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"NonCreaseScale")
			self.vertex_group_index+=1
			bops.deselectAll()

			for i in range(numvPleatVertices-1,-1,-2):
				bops.selectGroupVertices("CreaseScale"+str(i))
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"CreaseScale")
			self.vertex_group_index+=1
			bops.deselectAll()

			vm_plane.sortOnOneDimension((0,0,1),reverse=True)
			fromHere = vm_plane.getSorted(0,1)

			chosenVertices = vm_plane.selectChainFromVertexEvery(-1,fromHere[0],16,128)
			numvPleatVertices = len(chosenVertices)
			bops.deselectAll()

			for i,j in zip(range(numvPleatVertices-1,-1,-2),range(numvPleatVertices-2,-1,-2)):
				vm_plane.selectChainFromVertexEvery((0,1,0),chosenVertices[i],1,80)

				bops.addVertexGroup("Dhoti",self.vertex_group_index,"VerticalNonCreaseScale"+str(i))
				self.vertex_group_index+=1
				bops.deselectAll()

				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				vm_plane.sortOnOneDimension((0,0,1),reverse=True)
				fromHere = vm_plane.getSorted(0,1)
				chosenVertices = vm_plane.selectChainFromVertexEvery(-1,fromHere[0],16,128)
				bops.deselectAll()

				vm_plane.selectChainFromVertexEvery((0,1,0),chosenVertices[j],1,80)

				bops.addVertexGroup("Dhoti",self.vertex_group_index,"VerticalCreaseScale"+str(i))
				self.vertex_group_index+=1
				bops.deselectAll()

				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				vm_plane.sortOnOneDimension((0,0,1),reverse=True)
				fromHere = vm_plane.getSorted(0,1)
				chosenVertices = vm_plane.selectChainFromVertexEvery(-1,fromHere[0],16,128)
				bops.deselectAll()
			
				if j==1:
					vm_plane.selectChainFromVertexEvery((0,1,0),chosenVertices[0],1,80)

					bops.addVertexGroup("Dhoti",self.vertex_group_index,"VerticalNonCreaseScale"+str(0))
					self.vertex_group_index+=1
					bops.deselectAll()

			for i in range(numvPleatVertices-1,-1,-2):
				bops.selectGroupVertices("VerticalNonCreaseScale"+str(i))
			bops.selectGroupVertices("VerticalNonCreaseScale"+str(0))

			bops.addVertexGroup("Dhoti",self.vertex_group_index,"VNonCreaseScale")
			self.vertex_group_index+=1
			bops.deselectAll()

			for i in range(numvPleatVertices-1,0,-2):
				bops.selectGroupVertices("VerticalCreaseScale"+str(i))
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"VCreaseScale")
			self.vertex_group_index+=1
			bops.deselectAll()
		
			bops.selectVertices(chosenVertices)	
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"PinchScale")
			self.vertex_group_index+=1
			bops.deselectAll()

			bops.selectGroupVertices("PinchScale")
			bops.selectGroupVertices("WaistPin")
			bops.selectGroupVertices("NonCreaseScale")
			bops.selectGroupVertices("CreaseScale")
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"Allpin")
			self.vertex_group_index+=1

			bops.shadeSmooth()
			bops.makeItCloth("Dhoti","Allpin")
			self.drape(vm_plane)
			bops.editModeShape(self.name)
			bops.deselectAll()	

			return 0

		if simplePleat:
			bops.deselectAll()
			vm_plane.sortOnTwoDimensions((0,1,0),(0,0,1),reverse=True,reverse2=True)
			vm_plane.selectSorted(0,80)
			chosenVertices = vm_plane.selectEvery(int(80/4))	
			#vm_plane.selectEvery(4)	
			numPleatVertices = len(chosenVertices)

			for i in range(len(chosenVertices)):
				vm_plane.selectChainFromVertexByChange((0,0,-1),chosenVertices[i])

				bops.addVertexGroup("Dhoti",self.vertex_group_index,"HorizontalScale"+str(i))
				self.vertex_group_index+=1
				bops.deselectAll()

				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				vm_plane.sortOnTwoDimensions((0,1,0),(0,0,1),reverse=True,reverse2=True)
				vm_plane.selectSorted(0,80)
				chosenVertices = vm_plane.selectEvery(int(80/4))
				bops.deselectAll()

			for i in range(len(chosenVertices)):
				bops.selectGroupVertices("HorizontalScale"+str(i))

			bops.addVertexGroup("Dhoti",self.vertex_group_index,"HorizontalScale")
			self.vertex_group_index+=1
			bops.deselectAll()

			numCreases = 0
			for i in range(numPleatVertices-1):
				nthvertex = vm_plane.getnthfrom(chosenVertices[i],int(80/8),(0,-1,0))
				vm_plane.selectChainFromVertexByChange((0,0,-1),nthvertex)
				bops.addVertexGroup("Dhoti",self.vertex_group_index,"crease"+str(i))
				self.vertex_group_index+=1
				numCreases = i	
				bops.deselectAll()
				self.bm_p = bmesh.from_edit_mesh(bpy.context.object.data)
				vm_plane = _VerticesManager.verticesManager(self.bm_p.verts)

				vm_plane.sortOnTwoDimensions((0,1,0),(0,0,1),reverse=True,reverse2=True)
				vm_plane.selectSorted(0,80)
				chosenVertices = vm_plane.selectEvery(int(80/4))
				bops.deselectAll()

			nthvertex = vm_plane.getnthfrom(chosenVertices[0],int(80/8),(0,1,0))
			vm_plane.selectChainFromVertexByChange((0,0,-1),nthvertex)
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"crease"+str(numCreases+1))
			self.vertex_group_index+=1

			bops.deselectAll()
			for i in range(numPleatVertices):
				bops.selectGroupVertices('crease'+str(i))
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"creases")
			self.vertex_group_index+=1

			bops.deselectAll()	
			bops.selectGroupVertices('HorizontalScale')			
			bops.selectGroupVertices('WaistPin')			
			bops.selectGroupVertices('creases')			

			bops.addVertexGroup("Dhoti",self.vertex_group_index,"Allpin")
			self.vertex_group_index+=1

			bops.shadeSmooth()
			bops.makeItCloth("Dhoti","Allpin")
			self.simplePleatDrape(vm_plane)
			bops.editModeShape(self.name)
			bops.deselectAll()

		if kshtriya:
			bops.deselectAll()
			vm_plane.sortOnOneDimension((0,0,1),reverse=True)
			fromHere = vm_plane.getSorted(0,1)

			chosenVertices = vm_plane.selectChainFromVertexEvery(-1,fromHere[0],16,128)
			bops.selectVertices(chosenVertices)
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"RPinchScale")
			self.vertex_group_index+=1
			bops.deselectAll()

			vm_plane.sortOnOneDimension((0,1,0),reverse=True)
			fromHere = vm_plane.getSorted(1,2)

			chosenVertices = vm_plane.selectChainFromVertexEvery(-1,fromHere[0],16,128)
			bops.selectVertices(chosenVertices)
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"LPinchScale")
			self.vertex_group_index+=1
			bops.deselectAll()

			bops.selectGroupVertices("LPinchScale")
			bops.selectGroupVertices("RPinchScale")
			bops.selectGroupVertices("WaistPin")
			bops.addVertexGroup("Dhoti",self.vertex_group_index,"Allpin")
			self.vertex_group_index+=1

			bops.shadeSmooth()
			bops.makeItCloth("Dhoti","Allpin")
			self.kdrape(vm_plane)
			bops.editModeShape(self.name)
			bops.deselectAll()	

			return 0

class KeyPoints:
	def __init__(self):
		self.waist_width = -1
		self.waist_depth = -1
		self.height = -1
		self.base_width = -1
		
		self.width_middle = -1
		self.depth_middle = -1
		self.origin = -1

		self.tuckbackpoint = -1
		self.tuckbackbottom = -1

		self.waistBackPoint = -1
		self.waistFrontPoint = -1
		self.waistLeftPoint = -1
		self.waistRightPoint = -1
		self.midHeightApprox = -1
		self.frontMostPleat = -1
		self.numpleats = -1
			
		self.shoulderPoint = mathutils.Vector([0.35125,0.05176,4.54539])

class GetWidth(bpy.types.Operator):
	bl_label = "GetWidth"
	bl_idname = "mesh.cloth_width"

	def execute(self,context):
		bm = bmesh.from_edit_mesh(bpy.context.object.data)
		
		two_vertices = []
		for v in bm.verts:
			if v.select:
				two_vertices.append(v.co)

		distance = (two_vertices[0]-two_vertices[1]).length
		kp.waist_width = distance
	
		kp.width_middle = bpy.context.object.matrix_world*((two_vertices[0] + two_vertices[1]) / 2.0)

		if kp.depth_middle != -1:
			kp.origin = (kp.width_middle + kp.depth_middle) / 2.0

		return {'FINISHED'}

class GetDepth(bpy.types.Operator):
	bl_label = "GetDepth"
	bl_idname = "mesh.cloth_depth"

	def execute(self,context):
		bm = bmesh.from_edit_mesh(bpy.context.object.data)
		
		two_vertices = []
		for v in bm.verts:
			if v.select:
				two_vertices.append(v.co)

		distance = (two_vertices[0]-two_vertices[1]).length
		kp.waist_depth = distance

		kp.depth_middle = bpy.context.object.matrix_world*((two_vertices[0] + two_vertices[1]) / 2.0)

		if kp.width_middle != -1:
			kp.origin = (kp.width_middle + kp.depth_middle) / 2.0

		return {'FINISHED'}

class GetHeight(bpy.types.Operator):
	bl_label = "GetHeight"
	bl_idname = "mesh.cloth_height"

	def execute(self,context):
		bm = bmesh.from_edit_mesh(bpy.context.object.data)
		
		two_vertices = []
		for v in bm.verts:
			if v.select:
				two_vertices.append(v.co)

		distance = (two_vertices[0]-two_vertices[1]).length
		kp.height = distance
		

		return {'FINISHED'}

class GetBaseWidth(bpy.types.Operator):
	bl_label = "GetBaseWidth"
	bl_idname = "mesh.cloth_base_width"

	def execute(self,context):
		bm = bmesh.from_edit_mesh(bpy.context.object.data)
		
		two_vertices = []
		for v in bm.verts:
			if v.select:
				two_vertices.append(v.co)

		distance = (two_vertices[0]-two_vertices[1]).length
		kp.base_width = distance

		return {'FINISHED'}

class Draper(bpy.types.Operator):
	bl_label = "Draper"
	bl_idname = "mesh.draper"

	def execute(self,context):
		d = Dhoti(mathutils.Vector([-0.0018, -0.0770, 3.0832]),0.9327348857028808,0.6614779743555054,3.0544504039488944,1.6666979627849827,0.05,3,"Dhoti")
		#d = Dhoti(kp.origin,kp.waist_width,kp.waist_depth,kp.height,kp.base_width,0,3,"Dhoti")
		d.draw()

		return {'FINISHED'}
			
class ClothDimensions(bpy.types.Menu):
	bl_label = "Cloth Dimensions"
	bl_idname = "view3D.custom_menu"

	def draw(self, context):
		layout = self.layout

		layout.operator(GetWidth.bl_idname)
		layout.operator(GetDepth.bl_idname)
		layout.operator(GetHeight.bl_idname)
		layout.operator(GetBaseWidth.bl_idname)
		layout.operator(Draper.bl_idname)

def register():
	bpy.utils.register_class(GetWidth)
	bpy.utils.register_class(GetDepth)
	bpy.utils.register_class(GetHeight)
	bpy.utils.register_class(GetBaseWidth)
	bpy.utils.register_class(Draper)
	bpy.utils.register_class(ClothDimensions)

	kc = bpy.context.window_manager.keyconfigs.user
	km = kc.keymaps['3D View']
	kmi = km.keymap_items.new('wm.call_menu','Q','PRESS',shift=True)
	kmi.properties.name = ClothDimensions.bl_idname

	kmi.active = True

if __name__ == "__main__":
    register()

kp = KeyPoints()
motionMap = {}
#1
