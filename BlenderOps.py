import bpy,bmesh,os
import mathutils

class BlenderOps:
	groupIndices = {}
	def convertCurveToMesh(self):
		#convert curve to mesh
		self.setMode(mode='OBJECT')
		bpy.ops.object.convert(target='MESH')
		self.setMode(mode='EDIT')

	def extrudeOneAxis(self,axis,length):
		#extrude to make dhoti body 
		bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(axis[0]*length,axis[1]*length,axis[2]*length)})
		
	def setSelectMode(self,mode):
		bpy.ops.mesh.select_mode(type=mode)
		
	def setMode(self,mode):
		bpy.ops.object.mode_set(mode=mode)
		
	def selectAll(self):
		bpy.ops.mesh.select_all(action='SELECT')
		
	def deselectAll(self):
		bpy.ops.mesh.select_all(action='DESELECT')

	def selectTheseVertices(self,vertices,indices):
		self.setSelectMode('VERT')
		self.setMode('EDIT')

		for index in indices:
			vertices[index].select = True
	
	def selectTheseFaces(self,faces,indices):
		self.setSelectMode('FACE')
		self.setMode(mode='EDIT')
		
		for index in indices:
			faces[index].select = True
		
	def selectFaces(self,faces):
		self.setSelectMode('FACE')
		self.setMode(mode='EDIT')
		
		for index in range(len(faces)):
			faces[index].select = True
		
	def selectEdges(self,edges):
		self.setSelectMode('EDGE')
		self.setMode(mode='EDIT')
		
		for index in range(len(edges)):
			edges[index].select = True
			
	def selectVertices(self,vertices):
		self.setSelectMode('VERT')
		self.setMode(mode='EDIT')
		
		for index in range(len(vertices)):
			vertices[index].select = True
			
	def faceSubdivide(self,times):
		self.selectAll()
		for i in range(times):
			bpy.ops.mesh.subdivide()
		self.deselectAll()
		
	def addVertexGroup(self,objectName,index,groupName):
		bpy.ops.object.vertex_group_add()
		bpy.ops.object.vertex_group_assign()	

		bpy.data.objects[objectName].vertex_groups[index].name = groupName
		self.groupIndices[groupName] = index

	def removeFromVertexGroup(self,groupName):
		bpy.context.active_object.vertex_groups.active_index = self.groupIndices[groupName]
		bpy.ops.object.vertex_group_remove_from()

	def selectGroupVertices(self,name):
		index = 0
		for vg in bpy.context.active_object.vertex_groups:
			if vg.name == name:
				bpy.context.active_object.vertex_groups.active_index = index
				bpy.ops.object.vertex_group_select()
				break
			index+=1

	def selectAllGroups(self):
		index = 0
		for vg in bpy.context.active_object.vertex_groups:
			bpy.context.active_object.vertex_groups.active_index = index
			bpy.ops.object.vertex_group_select()
			index+=1

	def getActiveGroup(self):
		index = 0
		for vg in bpy.context.active_object.vertex_groups:
			if bpy.context.active_object.vertex_groups.active_index == index:
				return vg.name
			index+=1

	def makeItCloth(self,name,pin):
		bpy.ops.object.modifier_add(type='CLOTH')
		bpy.data.objects[name].modifiers["Cloth"].settings.use_pin_cloth = True
		bpy.data.objects[name].modifiers["Cloth"].settings.vertex_group_mass = pin
		bpy.data.objects[name].modifiers["Cloth"].collision_settings.use_self_collision = True
		'''
		bpy.data.objects[name].modifiers["Cloth"].collision_settings.self_collision_quality = 5
		bpy.data.objects[name].modifiers["Cloth"].collision_settings.collision_quality = 5
		bpy.types.PointCache.frame_end = 710	
		bpy.types.PointCache.use_disk_cache = True 
		'''	
	def bake(self):
		self.setMode('OBJECT')
		for scene in bpy.data.scenes:
			for object in scene.objects:
				for modifier in object.modifiers:
					if modifier.type == 'CLOTH':
						override = {'scene': scene, 'active_object': object, 'point_cache': modifier.point_cache}
						bpy.ops.ptcache.bake(override, bake=True)
						break 

	def shadeSmooth(self):
		self.setMode('OBJECT')
		bpy.ops.object.shade_smooth()
		self.setMode('EDIT')

	def createShapeKey(self):
		self.setMode('OBJECT')
		bpy.ops.object.shape_key_add(from_mix=False)
		self.setMode('EDIT')
		return bpy.data.shape_keys.keys()[-1]

	def addShapeKey(self):
		self.setMode('OBJECT')
		bpy.ops.object.shape_key_add(from_mix=False)
		self.setMode('EDIT')

	def setKeyValue(self,key,index,value):
		bpy.data.shape_keys[key].key_blocks[index].value = value

	def insertKeyframe(self,key,index,frame):
		bpy.data.shape_keys[key].key_blocks[index].keyframe_insert("value",frame=frame)

	def scale(self,value,constraint,proportional='DISABLED',proportional_size=1):
		bpy.ops.transform.resize(value=value,proportional=proportional,constraint_axis=constraint,proportional_edit_falloff='LINEAR',proportional_size=proportional_size)

	def translate(self,value,constraint):
		bpy.ops.transform.translate(value=value,constraint_axis=constraint)

	def rotate(self,value,axis,constraint):
		bpy.ops.transform.translate(value=value,axis=axis,constraint_axis=constraint,constraint_orientation='LOCAL')

	def mergeSelected(self):
		bpy.ops.mesh.merge(type='CENTER')

	def select(self,name):
		self.setMode('OBJECT')
		bpy.context.scene.objects.active = bpy.data.objects[name]
		self.setMode('EDIT')

	def selectObj(self,name):
		self.setMode('OBJECT')
		bpy.data.objects[name].select = True

	def deselect(self,name):
		self.setMode('OBJECT')
		bpy.data.objects[name].select = False

	def createPlane(self,location,rotation,width,height,name):
		self.setMode('OBJECT')
		bpy.ops.mesh.primitive_plane_add(radius=1,location=location,rotation=rotation)
		bpy.context.active_object.name = name
		bpy.ops.transform.resize(value=(width/2.0,1,height/2.0))
		self.setMode('EDIT')

	def createPlaneWithTexture(self,location,rotation,width,height,name):
		self.setMode('OBJECT')

		bpy.ops.mesh.primitive_plane_add(radius=1,location=location,rotation=rotation)
		bpy.context.active_object.name = name
		bpy.ops.transform.resize(value=(width/2.0,1,height/2.0))

		mat = bpy.data.materials.new('TexMat')

		realpath = os.path.expanduser('/home/sanjeevmk/Desktop/orange.jpg')
		img = bpy.data.images.load(realpath)
		cTex = bpy.data.textures.new('DhotiTex', type = 'IMAGE')
		cTex.image = img

		# Add texture slot for color texture
		mtex = mat.texture_slots.add()
		mtex.texture = cTex
		mtex.texture_coords = 'UV'
		mtex.use_map_color_diffuse = True 
		mtex.use_map_color_emission = True 
		mtex.emission_color_factor = 0.5
		mtex.use_map_density = True 
		mtex.mapping = 'FLAT'

		self.setMode('EDIT')
		bpy.ops.uv.smart_project()
		self.setMode('OBJECT')
	
		bpy.context.object.data.materials.append(mat)

		self.setMode('OBJECT')
		self.setMode('EDIT')

	def selectEdgeLoop(self):
		self.setMode('OBJECT')
		self.setMode('EDIT')
		bpy.ops.mesh.loop_multi_select(ring=False)
	
	def update(self):
		bpy.context.scene.update()

	def getWorld(self,name):
		self.select(name)
		return bpy.data.objects[name].matrix_world

	def delete(self,name):
		self.selectObj(name)
		bpy.ops.object.delete(use_global=False)

	def shortestPath(self):
		bpy.ops.mesh.shortest_path_select()

	def rotate(self,value,axis,constraint=(False,False,False)):
		bpy.ops.transform.rotate(value=value,axis=axis,constraint_axis=constraint)

	def editModeShape(self,name):
		bpy.data.objects[name].use_shape_key_edit_mode = True

	def setPivotPoint(self,mode,position=None):
		bpy.context.space_data.pivot_point = mode
		if mode=='CURSOR':
			bpy.context.scene.cursor_location = position

	def setTransformOrientation(self,orientation):
		bpy.context.space_data.transform_orientation = orientation

	def makeFace(self):
		bpy.ops.mesh.edge_face_add()
